# we want all the boutou to
# have a unique id
from enum import IntEnum
import numpy as np
from numpy import random

BOUTOU_INSTANCES = []

class Status(IntEnum):
    ALIVE = 1
    DEAD = 0


class Boutou:
    """Create a Boutou creature!

    Attributes
    ----------
    characteristics : dict
        a dictionnary of characteristics

    Methods
    -------
    Methods

    """
    def __init__(self, characteristics: dict):
        global BOUTOU_INSTANCES
        self.characteristics = characteristics

        self.instance = len(BOUTOU_INSTANCES)
        BOUTOU_INSTANCES.append(self)

        self.neurons = np.ones((2, 3))
        self.energy = 1.0                  # energy power
        self.status = Status.ALIVE         # dead or alive
        self.vision = random.rand()     # ability to view far
        self.direction = random.rand(2) # direction of movement
        self.position = random.rand(2)  # position
        self.speed = random.rand()      # speed

    def move(self):
        self.position += self.direction * self.speed # dt = 1


    def lifespan(self):
        """Compute the lifespan of the boutou
        """

    def __str__(self):
        return f'{self.get_name()}'

